package com.example.spinerpersonalisado;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;


public class SpinnerAdapter extends ArrayAdapter<ItemData> {

    int groupid;
    Activity Context;
    ArrayList<ItemData> list;
    LayoutInflater inflater;

    public SpinnerAdapter(Activity Context, int groupid, int id,ArrayList<ItemData>
            list){
        super (Context,id,list);
        this.list=list;
        inflater=
                (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid=groupid;}

    public View getView(int posicion, View convertView, ViewGroup parent) {

        View itemview = inflater.inflate(groupid, parent, false);
        ImageView imagen = (ImageView) itemview.findViewById(R.id.imgcategoria);
        imagen.setImageResource(list.get(posicion).getIdimagen());
        TextView textCategoria = (TextView) itemview.findViewById(R.id.lblcategorias);
        textCategoria.setText(list.get(posicion).getTxtcategoria());
        TextView textDescripcion = (TextView) itemview.findViewById(R.id.lbldescripcion);
        textDescripcion.setText(list.get(posicion).getTxtdescripcion());
        return itemview;
    }

    public View detDropDownView (int posicion,View convertView,ViewGroup parent) {
        return getView(posicion, convertView, parent);
    }

    }




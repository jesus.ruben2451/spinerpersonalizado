package com.example.spinerpersonalisado;

public class ItemData {
    private String txtcategoria;
    private String txtdescripcion;
    private  int idimagen;
//constructor
    public ItemData(String txrcategoria, String txtdescripcion, int idimagen) {
        this.txtcategoria = txrcategoria;
        this.txtdescripcion = txtdescripcion;
        this.idimagen = idimagen;
    }
//get
    public String getTxtcategoria() {
        return txtcategoria;
    }

    public String getTxtdescripcion() {
        return txtdescripcion;
    }

    public int getIdimagen() {
        return idimagen;
    }
//set
    public void setTxrcategoria(String txrcategoria) {
        this.txtcategoria = txrcategoria;
    }

    public void setTxtdescripcion(String txtdescripcion) {
        this.txtdescripcion = txtdescripcion;
    }

    public void setIdimagen(int idimagen) {
        this.idimagen = idimagen;
    }
}

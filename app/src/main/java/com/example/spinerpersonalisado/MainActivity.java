package com.example.spinerpersonalisado;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Spinner sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new
                ItemData(getString(R.string.itemfrappses),getString(R.string.msgfrappses),R.drawable.categorias));
        list.add(new
                ItemData(getString(R.string.itemagradecimiento),getString(R.string.msgagradecimiento),R.drawable.agradecimiento));
        list.add(new
                ItemData(getString(R.string.itemamor),getString(R.string.msgamor),R.drawable.amor));
        list.add(new
                ItemData(getString(R.string.itemnewyear),getString(R.string.msgnewyear),R.drawable.nuevo));
        list.add(new
                ItemData(getString(R.string.itemcancion),getString(R.string.msgcanciones),R.drawable.canciones));
        sp=(Spinner) findViewById(R.id.spinner1);
        SpinnerAdapter adapter=new
                SpinnerAdapter(this,R.layout.spinner_layout,R.id.lblcategorias,list);
        sp.setAdapter(adapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(adapterView.getContext(),getString(R.string.msgseleccionado).toString() +""+((ItemData)
                        adapterView.getItemAtPosition(i)).getTxtcategoria(),
                        Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
